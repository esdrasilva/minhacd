package br.com.esdras.minhacd.model;

import java.util.Objects;

public class ComponenteCurricular {
    private Integer id;
    private String componente;
    private Modulo modulo;

    public ComponenteCurricular(Integer id, String componente, Modulo modulo) {
        this.id = id;
        this.componente = componente;
        this.modulo = modulo;
    }

    public ComponenteCurricular() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComponente() {
        return componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponenteCurricular that = (ComponenteCurricular) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ComponenteCurricular{" +
                "id=" + id +
                ", componente='" + componente + '\'' +
                ", modulo=" + modulo +
                '}';
    }
}
