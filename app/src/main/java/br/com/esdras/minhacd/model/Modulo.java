package br.com.esdras.minhacd.model;

public enum Modulo {
    PRIMEIRO(0),
    SEGUNDO(1),
    TERCEIRO(2);

    private int id;

    Modulo(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    /**
     * Metodo responsavel por recuperar um item do enum conforme o id informado.
     * @param id - Integer
     * @return Modulo
     */
    public static Modulo toEnum(Integer id) {
        for (Modulo m : Modulo.values()) {
            if (id.equals(m.getId())) return m;
        }
        throw new IllegalArgumentException("Id inválido: " + id);
    }
}
