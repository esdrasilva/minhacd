package br.com.esdras.minhacd.model;

public enum Eixo {
    INFORMCAO_E_COMUNICACA("Informação e Comunicação"),
    GESTAO_E_NEGOCIOS("gestão e Negócios");

    private String eixo;

    Eixo(String eixo) {
        this.eixo = eixo;
    }

    public String getEixo() {
        return eixo;
    }
}
