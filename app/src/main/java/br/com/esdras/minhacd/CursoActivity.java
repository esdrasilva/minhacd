package br.com.esdras.minhacd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.esdras.minhacd.model.Curso;

public class CursoActivity extends AppCompatActivity {

    TextView textViewCursoSelecionado, textViewDescricaoCurso, textViewS1, textViewS2, textViewS3;
    ImageView imageView;
    int posicaoCurso = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curso);

        //Recuperar a posicao do curso selecionado na tela anterior
        posicaoCurso = getIntent().getIntExtra("posicao", 0);

        //Recuperar os dados do curso no DataSource conforme sua posição
        Curso curso = MainActivity.cursos.get(posicaoCurso);


        //Databinding - Preenchimento de dados na interface grafica
        imageView = findViewById(R.id.imageViewCursoSelecionado);
        imageView.setImageResource(curso.getImageResourceId());

        textViewCursoSelecionado = findViewById(R.id.textViewCursoSelecionado);
        textViewCursoSelecionado.setText(curso.getCurso());

        textViewDescricaoCurso = findViewById(R.id.textViewDescricaoCurso);
        textViewDescricaoCurso.setText(curso.getDescricao());

        textViewS1 = findViewById(R.id.textViewS1);
        textViewS1.setText("Habilitação: "+curso.getHabilitacao().get(0));

        textViewS2 = findViewById(R.id.textViewS2);
        textViewS2.setText("Habilitação: "+curso.getHabilitacao().get(1));

        textViewS3 = findViewById(R.id.textViewS3);
        textViewS3.setText("Habilitação: "+curso.getHabilitacao().get(2));
    }

    /**
     * Metodo responsavel por fazer a transicao para a tela de componentes curriculares
     * conforme o modulo selecionado[pimeiro, segundo ou terceiro].
     * @param view
     */
    public void abrirComponentes(View view) {
        int posicao = 0;
        switch (view.getId()) {
            case R.id.linearLayoutPS:
                posicao = 0;
                break;
            case R.id.linearLayoutSS:
                posicao = 1;
                break;
            case R.id.linearLayoutTS:
                posicao = 2;
                break;
        }


        // Objeto Bundle paa agrupar os dados a serem enviados.
        Bundle bundle = new Bundle();
        bundle.putInt("modulo", posicao);
        bundle.putInt("curso", posicaoCurso);

        Intent intent = new Intent(this, ComponentesActivity.class);
        //Adicao do bundle à Intent
        intent.putExtras(bundle);
        startActivity(intent);
    }
}