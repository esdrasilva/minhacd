package br.com.esdras.minhacd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Curso {
    private Integer id;
    private Integer imageResourceId;
    private String curso;
    private String descricao;
    private Eixo eixo;
    private List<ComponenteCurricular> componentes = new ArrayList<>();
    private List<String> habilitacao = new ArrayList<>();

    public Curso(Integer id, String curso, String descricao, Integer imageResourceId, Eixo eixo) {
        this.id = id;
        this.curso = curso;
        this.descricao = descricao;
        this.eixo = eixo;
        this.imageResourceId = imageResourceId;
    }

    public Curso() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Eixo getEixo() {
        return eixo;
    }

    public void setEixo(Eixo eixo) {
        this.eixo = eixo;
    }

    public List<ComponenteCurricular> getComponentes() {
        return componentes;
    }

    public void setComponentes(List<ComponenteCurricular> componentes) {
        this.componentes = componentes;
    }

    public Integer getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(Integer imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public List<String> getHabilitacao() {
        return habilitacao;
    }

    public void setHabilitacao(List<String> habilitacao) {
        this.habilitacao = habilitacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Curso curso = (Curso) o;
        return id.equals(curso.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Curso{" +
                "id=" + id +
                ", imageResourceId=" + imageResourceId +
                ", curso='" + curso + '\'' +
                ", descricao='" + descricao + '\'' +
                ", eixo=" + eixo +
                ", componentes=" + componentes +
                ", habilitaçao=" + habilitacao +
                '}';
    }
}
