package br.com.esdras.minhacd;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.List;
import java.util.stream.Collectors;

import br.com.esdras.minhacd.model.ComponenteCurricular;
import br.com.esdras.minhacd.model.Curso;
import br.com.esdras.minhacd.model.Modulo;

//TODO: Alterar para [ extedns AppCOmpatActivity ]
public class ComponentesActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: Reaproveitar o layout abaixo, adicionando uma ListView nele...
//        setContentView(R.layout.activity_componentes);

        //Recuperar os dados de curso e modulo no bundle.
        Bundle bundleExtra = getIntent().getExtras();

        //Recuperar o curso no DataSource
        Curso curso = MainActivity.cursos.get(bundleExtra.getInt("curso"));

        // Obter a lista de disciplinas filtrando a partir da lista de componentes do curso
        // selecionado
        List<String> componentes = curso.getComponentes().stream()
                .filter(o -> o.getModulo().equals(Modulo.toEnum(bundleExtra.getInt("modulo"))))
                .map(ComponenteCurricular::getComponente)
                .collect(Collectors.toList());

        //TODO: Criar uma nova ListView estilizada para exibir as disciplinas

        //Criacao do Adapter para a lista de disciplinas.
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, componentes);
        setListAdapter(arrayAdapter);
    }
}