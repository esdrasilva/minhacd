package br.com.esdras.minhacd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.esdras.minhacd.model.ComponenteCurricular;
import br.com.esdras.minhacd.model.Curso;
import br.com.esdras.minhacd.model.Eixo;
import br.com.esdras.minhacd.model.Modulo;

public class MainActivity extends AppCompatActivity {

    // Atributo que agira como DataSource.
    public static List<Curso> cursos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String lorem = getString(R.string.lorem);

        //Inicializacao da do DataSource
        cursos = new ArrayList<>();

        // Criacao do primeiro objeto para a nossa lista de cursos, o DataSource
        Curso sistemas = new Curso(1, "Desenvolvimento de Sistemas", lorem, R.drawable.dev, Eixo.INFORMCAO_E_COMUNICACA);

        // Criacao dos componentes curriculares para o curso de sistemas
        ComponenteCurricular dd = new ComponenteCurricular(1, "Design Digital", Modulo.PRIMEIRO);
        ComponenteCurricular tp = new ComponenteCurricular(2, "Tecnicas de Programação", Modulo.SEGUNDO);
        ComponenteCurricular ap = new ComponenteCurricular(3, "Algoritmos e Programação", Modulo.TERCEIRO);
        //TODO: Incluam mais disciplinas ao nosso curso, nos tres modulos se possivel

        // Criacao de lista com os componentes curriculares criados
        //TODO: Adicionem os objetos das disciplinas nesta lista...
        List<ComponenteCurricular> sistemasListComponentes = Arrays.asList(dd, tp, ap);

        //Adicao da lista ao objeto sistemas
        sistemas.setComponentes(sistemasListComponentes);
        sistemas.setHabilitacao(Arrays.asList("Auxiliar em Desenvolvimento de Sistemas", "Programador de Computadores", "Técnico em Desenvolvimento de Sistemas"));


        //SImplificacao do processo de criacao de objetos utilizando o metodo addAll e asList...
        Curso administracao = new Curso(2, "Administração", lorem, R.drawable.administracao, Eixo.GESTAO_E_NEGOCIOS);
        administracao.getComponentes().addAll(Arrays.asList(
                new ComponenteCurricular(1, "Aplicativos Informatizados", Modulo.PRIMEIRO),
                new ComponenteCurricular(2, "DTCC", Modulo.SEGUNDO),
                new ComponenteCurricular(3, "LTT", Modulo.TERCEIRO)
        ));
        administracao.setHabilitacao(Arrays.asList("Habilitação I", "Habilitação II", "Habilitação III"));

        Curso logistica = new Curso(3, "Logística", lorem, R.drawable.logistica, Eixo.GESTAO_E_NEGOCIOS);
        logistica.getComponentes().addAll(Arrays.asList(
                new ComponenteCurricular(1, "Aplicativos Informatizados", Modulo.PRIMEIRO),
                new ComponenteCurricular(2, "DTCC", Modulo.SEGUNDO),
                new ComponenteCurricular(3, "LTT", Modulo.TERCEIRO)
        ));
        logistica.setHabilitacao(Arrays.asList("Habilitação I", "Habilitação II", "Habilitação III"));


        Curso marketing = new Curso(4, "Marketing", lorem, R.drawable.marketing, Eixo.GESTAO_E_NEGOCIOS);
        marketing.getComponentes().addAll(Arrays.asList(
                new ComponenteCurricular(1, "Aplicativos Informatizados", Modulo.PRIMEIRO),
                new ComponenteCurricular(2, "DTCC", Modulo.SEGUNDO),
                new ComponenteCurricular(3, "LTT", Modulo.TERCEIRO)
        ));
        marketing.setHabilitacao(Arrays.asList("Habilitação I", "Habilitação II", "Habilitação III"));

        // Adicao de todos os cursos com seus respectivos dados criados.
        // O DataSource esta pronto!!!!!!!
        cursos.addAll(Arrays.asList(administracao, sistemas, logistica, marketing));
    }

    /**
     * Metodo responsavel por fazer a chamada para activity do sistema para realizar uma ligacao.
     * @param view
     */
    public void fazerLigacao(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+551935412819"));
        startActivity(intent);
    }
    /**
     * Metodo responsavel por fazer a chamada para activity do sistema para abrir
     * o site da Etec no vanegador padrão do sistema.
     * @param view
     */
    public void abrirSiteEtec(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.albertoferes.com.br"));
        startActivity(intent);
    }

    /**
     * Metodo responsavel por fazer a chamada para activity do sistema para
     * realizar o envio de um e-mail com os apps com a capacidade de fazer o envio.
     * @param view
     */
    public void enviarEmail(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        String[] to = {"e024admin@cps.sp.gov.br", "contato@albertoferes.com.br"};
        intent.putExtra(Intent.EXTRA_EMAIL, to);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Gostaria de saber um pouco mais sobre a Etec.");
        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent, "Email"));
    }

    /**
     * Metodo responsavel por abrir a tela de detalhe de curso combase na posição do CardView selecionado,
     * enviando a posição para CursoActivity.
     * @param view
     */
    public void abrirDetalheCurso(View view) {

        int posicao = 0;
        switch (view.getId()) {
            case R.id.cardViewAdminstracao:
                posicao = 0;
                break;
            case R.id.cardViewDevSistemas:
                posicao = 1;
                break;
            case R.id.cardViewLogistica:
                posicao = 2;
                break;
            case R.id.cardViewMarketing:
                posicao = 3;
                break;
        }

        Intent intent = new Intent(this, CursoActivity.class);

        // Adicionando o apoisicao selecionada para o envio.
        intent.putExtra("posicao", posicao);
        startActivity(intent);
    }
}